# HEXACONF

**Version statique**


## Membres de l'�quipe :

Vincent *PRIGENT*

R�mi *BERTIN*

Souleymane *DIABY*

Gustavo *PEZA MENDEZ*

Jordan *PUISSEGUR*

## Qu'est-ce que Hexaconf/version statique ?

Le d�p�t Hexaconf version statique est un d�p�t regroupant le code source des diff�rentes parties (pages) du site. De plus, on peut y avoir les modifications et corrections apport�s aux pages par les membres de l'�quipe. 


## Le site statique, � quoi sert t'il ?

Le site dans sa version statique consiste � d�velopper chaque page du site (page acceuil, page saisie de conf�rence, ...etc) et � int�grer le contenu afin de donner un avant go�t du site final et de pouvoir le pr�senter.
De plus, le site utilise la technologie AJAX et doit par cons�quent  fonctionner sur un serveur web.


## Technologies utilis�es :

- Framework front-end : Foundation

- AJAX



