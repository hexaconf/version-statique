var map;
function initMap() {
    var width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 46.5967, lng: 2.8733205},
        zoom: 5
    });

    addMarker(44.806404, -0.606640, "BDX I/O", "#bdx-io", map);
    addMarker(43.230119, 5.443246, "Alea 2016", "#alea-2016", map);
    addMarker(48.821486, 2.320754, "Paris Web", "#paris-web", map);
}
function addMarker(lat, long, text, link, map) {
    var marker = new google.maps.Marker({
        position: {
            lat: lat,
            lng: long
        },
        map: map
    });
    attachInfoWindow(marker, text, link)
}
function attachInfoWindow(marker, text, link) {
    var resultingText;
    if (typeof link != 'undefined') {
        resultingText = "<a href='"+link+"'>"+text+"</a>"
    }
    else resultingText = text;
    var infowindow = new google.maps.InfoWindow({
        content: resultingText
    });
    marker.addListener('click', function() {
        infowindow.open(marker.get('map'), marker);
    });
}