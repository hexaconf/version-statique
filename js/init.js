$(document).ready(function() {

$(document).foundation();

var elem = document.querySelector('.grid');
var $grid = $('.grid').isotope({
  layoutMode: 'fitRows',
  filter: '*'
});

$('.filter-button-group').on( 'click', 'button', function() {
  var filterValue = $(this).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});
});